import requests
import json
import socket


ports = dict(web1=80, web2=8085, auto_rest=8091, rest_ip='127.0.0.1')


def get_ip():
    hostname = socket.gethostname()
    ip = socket.gethostbyname(hostname)
    print(f'[{hostname} | {ip}]', end='')
    return ip


def set_data(login='user', password='pass', ip=ports.get('rest_ip'), port=ports.get('auto_rest')):
    auth = f'{login}:{password}'
    host = f'{ip}:{port}'
    file = r'D:\!work\!neuro\smoke_dpe187_openvino\smoke_dpe187_openvino.ann'
    key = r'D:\!work\!neuro\smoke_dpe187_openvino\smoke_dpe187_openvino.annCPU'
    return auth, host, file, key


def update_server(auth, host, file, key):
    url = f'http://{auth}@{host}/UpdateServer'
    payload = {"device": "CPU",  # Устройство, на котором работает нейросеть: CPU или GPU
               "file": f'{file}',  # Полный путь до файла обученной нейросети
               "sensitivity": 65,  # Чувствительность распознавания
               "key": f'{key}',  # Произвольный уникальный ключ. Необходим для предотвращения повторной загрузки файла нейросети.
               "lprid": "1"}  # Идентификатор Канала распознавания номеров в ПК Авто-Интеллект

    headers = {'Content-type': 'application/json',
               'Accept': 'text/plain',
               'Content-Encoding': 'utf-8'}

    response = requests.request("POST", url, headers=headers, data=json.dumps(payload))
    parsed = json.loads(response.content)
    for dict_n in parsed:
        print('\n')
        for item in dict_n.items():
            print(item)


update_server(*set_data())
